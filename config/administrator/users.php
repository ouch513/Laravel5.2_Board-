<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 6/8/16
 * Time: 15:27
 */

return [
    'title' =>  "User",

    'single' => "User",

    'model' => "Cartalyst\\Sentinel\\Users\\EloquentUser",

    'columns' => [
        'id' => ['title'=>'ID'],
        'email' => ['title'=>'email'],
        'username' => ['title'=>'username'],
        'firstname' => ['title'=>'firstname'],
        'lastname' => ['title'=>'lastname'],
    ],

    'edit_fields' => [
        'email' => [
            'title' => 'email',
            'type' => 'text'
        ],
        'username' => [
            'title' => 'username',
            'type' => 'text'
        ],
        'firstname' => [
            'title' => 'firstname',
            'type' => 'text'
        ],
        'lastname' => [
            'title' => 'lastname',
            'type' => 'text'
        ],
    ],
];
