<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 6/1/16
 * Time: 21:56
 */

return [
    'title' =>  "Role",

    'single' => "Role",

    'model' => "Cartalyst\\Sentinel\\Roles\\EloquentRole",

    'columns' => [
        'id' => ['title'=>'ID'],
        'slug' => ['title'=>'slug'],
        'name' => ['title'=>'name'],
//        'permissions' => ['title'=>'permissions'],
    ],

    'edit_fields' => [
        'slug' => [
            'title' => 'Slug',
            'type' => 'text'
        ],
        'name' => [
            'title' => 'Name',
            'type' => 'text'
        ],

    ],
];