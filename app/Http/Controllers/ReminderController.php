<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 5/27/16
 * Time: 21:05
 */

namespace App\Http\Controllers;

use App\Events\ReminderEvent;
use App\Http\Requests;
use Event;
use Flash;
use Illuminate\Support\Facades\Input;
use Redirect;
use Reminder;
use App\Repositories\DbUserRepository as UserRepository;
use Session;
use View;

class ReminderController extends Controller
{
    public function __construct(UserRepository $userRepository)
    {
        $this->users = $userRepository;
    }

    public function create() {
        return View::make('reminders.create');
    }

    public function store() {
        $login = Input::get('login');

//        $user = $this->users->findByLogin($login);
        $user = $this->users->findByLogin($login);

        if ($user) {
            ($reminder = Reminder::exists($user)) || ($reminder = Reminder::create($user));

            Event::fire(new ReminderEvent($user, $reminder));
        }

        return View::make('reminders.store');
    }

    public function edit($id, $code) {
        $user = $this->users->findById($id);

        if (Reminder::exists($user, $code)) {
            return View::make('reminders.edit', ['id' => $id, 'code' => $code]);
        }
        else {
            //incorrect info was passed
            return Redirect::to('/');
        }
    }

    public function update($id, $code) {
        $password = Input::get('password');
        $passwordConf = Input::get('password_confirmation');

        $user = $this->users->findById($id);
        $reminder = Reminder::exists($user, $code);

        //incorrect info was passed.
        if ($reminder == false) {
            Flash::error('Password reset failed');
            return Redirect::to('/');
        }

        if ($password != $passwordConf) {
            Session::flash('error', 'Passwords must match.');
            return View::make('reminders.edit', ['id' => $id, 'code' => $code]);
        }

        Reminder::complete($user, $code, $password);

        Flash::success('Password reset success');
        return Redirect::to('/');
    }
}