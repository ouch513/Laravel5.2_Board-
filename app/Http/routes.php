<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/', function () {
    return view('welcome');
});
 */


/*
|--------------------------------------------------------------------------
| API routes
|--------------------------------------------------------------------------
*/

Route::group(['prefix' => 'api', 'namespace' => 'API'], function () {
    Route::group(['prefix' => 'v1'], function () {
        require config('infyom.laravel_generator.path.api_routes');
    });
});


# Static Pages. Redirecting admin so admin cannot access these pages.
Route::group(['middleware' => ['redirectAdmin']], function()
{
    Route::get('/', ['as' => 'home', 'uses' => 'PagesController@getHome']);
    Route::get('about', ['as' => 'about', 'uses' => 'PagesController@getAbout']);
    Route::get('contact', ['as' => 'contact', 'uses' => 'PagesController@getContact']);
});

# Registration
Route::group(['middleware' => 'guest'], function()
{
    Route::get('register', 'RegistrationController@create');
    Route::post('register', ['as' => 'registration.store', 'uses' => 'RegistrationController@store']);
});

# Authentication
Route::get('login', ['as' => 'login', 'middleware' => 'guest', 'uses' => 'SessionsController@create']);
Route::get('logout', ['as' => 'logout', 'uses' => 'SessionsController@destroy']);
Route::resource('sessions', 'SessionsController' , ['only' => ['create','store','destroy']]);

# Forgotten Password [Old]
Route::group(['middleware' => 'guest'], function()
{
//    Route::get('forgot_password', 'Auth\PasswordController@getEmail');
//    Route::post('forgot_password','Auth\PasswordController@postEmail');
//    Route::get('reset_password/{token}', 'Auth\PasswordController@getReset');
//    Route::post('reset_password/{token}', 'Auth\PasswordController@postReset');

    Route::get('passwordreset/{id}/{token}', ['as' => 'reminders.edit', 'uses' => 'ReminderController@edit']);
    Route::post('passwordreset/{id}/{token}', ['as' => 'reminders.update', 'uses' => 'ReminderController@update']);
    Route::get('passwordreset', 'ReminderController@create');
    Route::post('passwordreset', 'ReminderController@store');
});



# Standard User Routes
Route::group(['middleware' => ['auth','standardUser']], function()
{
    Route::get('userProtected', 'StandardUser\StandardUserController@getUserProtected');
    Route::resource('profiles', 'StandardUser\UsersController', ['only' => ['show', 'edit', 'update']]);
});

# AdminUser Routes
Route::group(['middleware' => ['auth', 'admin']], function()
{
    Route::get('admins', ['as' => 'admins_dashboard', 'uses' => 'AdminUser\AdminController@getHome']);
    Route::resource('admins/profiles', 'AdminUser\AdminUsersController', ['only' => ['index', 'show', 'edit', 'update', 'destroy']]);
});

Route::get('auth/weibo', 'Auth\AuthController@weibo');
Route::get('auth/weiboCallback', 'Auth\AuthController@weiboCallback');

Route::get('auth/qq', 'Auth\AuthController@qq');
Route::get('auth/QQCallback', 'Auth\AuthController@QQCallback');

Route::get('auth/weixin', 'Auth\AuthController@weixin');
Route::get('auth/weixinCallback', 'Auth\AuthController@weixinCallback');
