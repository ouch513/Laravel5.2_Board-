<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 5/27/16
 * Time: 21:09
 */

namespace App\Events;

use App\Models\User;
use Illuminate\Queue\SerializesModels;
use Cartalyst\Sentinel\Reminders\EloquentReminder as Reminder;

class ReminderEvent extends Event
{
    use SerializesModels;
    public $user;
    public $reminder;

    public function __construct(User $user, Reminder $reminder)
    {
        $this->user = $user;
        $this->reminder = $reminder;
    }
}