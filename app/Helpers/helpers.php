<?php


function errors_for($attribute, $errors)
{
    return $errors->first($attribute, '<p class="text-danger">:message</p>');
}

function set_active($path, $active = 'active')
{
    // return Request::is($path) || Request::is($path . '/*') ? $active: '';
    return Request::is($path) || Request::is($path . '/*') ? $active : '';
}

function set_active_admin($path, $active = 'active')
{
    return Request::is($path) ? $active : '';
}

function authorize_admin()
{
    if ($user = Sentinel::check())
    {
        // Check if the user has access to the admin page
        $admin = Sentinel::findRoleByName('Admins');
        if (Sentinel::getUser()->inRole($admin))
            return true;
    }
    return false;
}