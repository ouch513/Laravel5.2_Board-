<?php
/**
 * Created by PhpStorm.
 * User: home
 * Date: 5/27/16
 * Time: 21:10
 */

namespace App\Listeners;

use App\Events\ReminderEvent;
use Mail;

class ReminderEmailSender
{
    public function handle(ReminderEvent $event)
    {
        $user = $event->user;
        $reminder = $event->reminder;

        $data = [
            'email' => $user->email,
            'name' => 'AdminUser',
            'subject' => 'Reset Your Password',
            'code' => $reminder->code,
            'id' => $user->id
        ];

        Mail::queue('emails.reminder', $data, function($message) use ($data) {
            $message->to($data['email'], $data['name'])->subject($data['subject']);
        });
    }
}