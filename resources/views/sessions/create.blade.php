@extends('layouts.app')

@section('title', 'Login')

@section('content')


  <div class="container">
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title">Login</h3>
          </div>
          <div class="panel-body">
            {!! Form::open(['route' => 'sessions.store']) !!}
            <fieldset>

              @if (session()->has('flash_message'))
                <div class="alert alert-success">
                  {{ session()->get('flash_message') }}
                </div>
              @endif

              @if (session()->has('error_message'))
                <div class="alert alert-danger">
                  {{ session()->get('error_message') }}
                </div>
              @endif

              <!-- Email field -->
              <div class="form-group">
                {!! Form::text('login', null, ['placeholder' => 'Email or User Name', 'class' => 'form-control', 'required' => 'required'])!!}
                {!! errors_for('login', $errors) !!}
              </div>

              <!-- Password field -->
              <div class="form-group">
                {!! Form::password('password', ['placeholder' => 'Password','class' => 'form-control', 'required' => 'required'])!!}
                {!! errors_for('password', $errors) !!}
              </div>

              <div class="checkbox">
                <!-- Remember me field -->
                <div class="form-group">
                  <label>
                    {!! Form::checkbox('remember', 'remember') !!} Remember me
                  </label>
                </div>
              </div>

              <!-- Submit field -->
              <div class="form-group">
                {!! Form::submit('Login', ['class' => 'btn btn btn-lg btn-success btn-block']) !!}
              </div>
            </fieldset>
            {!! Form::close() !!}
             <p align="right">
        <a class="btn btn-default" style="width:40px;height:40px;;padding:0;" href="{{ url('auth/weibo') }}" title="新浪微博">
        <img style="width:100%;height:100%;border:0;margin:0;" src="{{ URL::asset('/') }}img/linkweibo.png" />
                </a>
        <a class="btn btn-default" style="width:40px;height:40px;padding:0;"  href="{{ url('auth/qq') }}" title="QQ帐号">
        <img style="width:100%;height:100%;border:0;margin:0;" src="{{ URL::asset('/') }}img/linkqq.png" />
                </a>
       <a class="btn btn-default" style="width:40px;height:40px;padding:0;" href="{{ url('auth/weixin') }}" title="微信帐号">
        <img style="width:100%;height:100%;border:0;margin:0;" src="{{ URL::asset('/') }}img/linkweixin.png" />
       </a>
             </p>

          </div>
        </div>
        <div style="text-align:center">
          <p><a href="{{ url('forgot_password') }}">Forgot Password?</a></p>
        </div>


      </div>
    </div>
  </div>

@endsection
