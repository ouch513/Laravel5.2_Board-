@extends('master')

@section('title', 'Reset Email has been sent to your email box')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-3">
                Reset email has been sent.
                Please check your email box.
                If not received reset mail, please check your spam settings.
            </div>
        </div>
    </div>

@endsection
