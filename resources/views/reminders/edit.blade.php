@extends('layouts.app')

@section('title', 'Reset')

@section('content')

{!! Form::open(['reminders.update', $id, $code]) !!}
{!! Form::password('password', array('placeholder'=>'new password', 'required'=>'required')) !!}
{!! Form::password('password_confirmation', array('placeholder'=>'new password confirmation', 'required'=>'required')) !!}
{!! Form::submit('Reset Password') !!}
{!! Form::close() !!}

@endsection