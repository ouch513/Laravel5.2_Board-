<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class SentinelUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();

        Sentinel::registerAndActivate([
            'username' => 'user',
            'email'    => 'user@user.com',
            'password' => '123456',
            'first_name' => 'UserFirstName',
            'last_name' => 'UserLastName',
        ]);

        Sentinel::registerAndActivate([
            'username' => 'admin',
            'email'    => 'jjhome@guet.edu.cn',
            'password' => '123456',
            'first_name' => 'AdminFirstName',
            'last_name' => 'AdminLastName',
        ]);

        $this->command->info('Users seeded!');

    }
}
