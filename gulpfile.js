var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

// Set your Bower target directory below.
var bowerDir = './vendor/bower_components';

// Helper function.
function bower(pkg, path) {
    return [bowerDir, pkg, path].join('/');
}

elixir(function(mix) {
    // Styles
    mix.sass('app.scss', null, {includePaths: [bowerDir]});

    // Fonts
    mix.copy(bower('bootstrap-sass', 'assets/fonts/**'), 'public/build/fonts');


    // Scripts
    mix.scripts([
        bower('jquery', 'dist/jquery.min.js'),
        bower('bootstrap-sass', 'assets/javascripts/bootstrap.min.js')
    ]);
    mix.copy(bower('html5shiv', 'dist/html5shiv.min.js'), 'public/js');
    mix.copy(bower('respond', 'dest/respond.min.js'), 'public/js');

    // Versioning
    mix.version([
        'css/app.css',
        'js/all.js'
    ]);
});
