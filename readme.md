## 1.克隆项目
	git clone https://git.oschina.net/ouch513/Laravel5.3_Board.git


## 2.本地配置

	(1)run: composer install
	
	(2)create database in mysql

	(3)set up .env file
		设置要连接的数据库名，与本机数据库用户名和密码

	(4)php artsian key:generate


## 3.确保/storage下有以下文件夹，没有则自行创建

	app
	framework/cache
	framework/sessions
	framework/views
	logs

	
## 4.确保已安装node和npm

	node -v
	
	npm -v

	npm install --global gulp-cli

	npm install -g cnpm --registry=https://registry.npm.taobao.org

	cnpm install
   
	gulp

## 5.数据迁移与填充

	php artisan migrate:refresh --seed

## 6.运行测试

	run: php artisan serve

	open http://localhost:8000 at browser
